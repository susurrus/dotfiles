set nocompatible      " Don't force vi compatibility

" Enable syntax highlighting and use the dark colorscheme.
filetype plugin indent on
syntax enable
set background=dark

set hlsearch " Highlight search results
nnoremap <CR> :nohlsearch<CR> " Clear search results using RETURN

set spell spelllang=en_us " Enable spell-checking

set mouse=a " Support using the mouse

" Make backspace able to delete any text
set backspace=2
set backspace=indent,eol,start

" Enable line numbers for common source code files
autocmd FileType python setlocal number

" Hide buffers when they are abandoned, so you can switch buffers without saving
set hidden

" Have Vim jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

nnoremap ; : " Switch the colon operator to start VIm commands with semicolon

cmap w!! w !sudo tee % >/dev/null " Allow for sudo writing a file using "w!!" command

set autowrite		" Automatically save before commands like :next and :make
set autoindent      " Preserve indentation of parent line when inserting a new line
set expandtab
set copyindent      " Copy the previous indentation on autoindenting
set shiftwidth=4    " Set the number of spaces for autoindenting
set shiftround      " Use multiples of shiftwidth when manually indenting
set ignorecase		" Do case insensitive matching
set incsearch		" Incremental search
set noerrorbells    " Disable error bells (normally disabled I think)
set scrolloff=5     " When scrolling with the cursor, keep the surrounding 5 lines visible
set showcmd		    " Show (partial) command in status line.
set showmatch		" Show matching brackets.
set smartcase		" Do smart case matching
set smarttab        " Use shiftwidth to specify indentation instead of tabstop
set history=1000    " Remember more history
set undolevels=1000 " Remember lots of stuff to undo
set wildignore=*.swp,*.bak,*.pyc,*~ " Ignore some files I'll never open in VIm
set title           " Vim sets the terminal title to the current open buffer
set visualbell      " Don't beep, flash the screen instead
set noerrorbells    " Don't beep on error
set list            " Show some whitespace characters
set listchars=tab:>.,trail:.,extends:#,nbsp:. " Display tabs, long lines, spaces, and trails

set pastetoggle=<F2> " Toggle paste mode with F2. Useful for pasting text so that autoindentation doesn't kick in

" Support copying to the clipboard
set clipboard+=autoselect " Automatically copy highlighted text in non-GUI mode
set clipboard+=unnamedplus " Specify the X11 clipboard as where text should be copied to
set guioptions+=a " Auto-copy highlighted text in GUI mode

" Automatically remove trailing whitespace before saving
autocmd BufWritePre * :%s/\s\+$//e

" Enable powerline
python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup
set laststatus=2
set t_Co=256

" Set font to Hack
set gfn=Hack

" Add the Pathogen plugin manager
execute pathogen#infect()

" Turn on 'hidden' mode for buffers to support Rust's racer integration
set hidden

" Support racer for Rust support
let g:racer_cmd = "racer"

set clipboard+=unnamed " use the clipboards of vim and the display manager
set paste              " Paste from the clipboard
set go+=a              " Visual selection automatically copied to clipboard
